import java.util.*;

public class StreamSelector {

    public static void getComputerGroupStudent(String name,ArrayList<String> computerGroup){
        computerGroup.add(name);
    }

    public static void getMathsGroupStudent(String name,ArrayList<String> mathsGroup){
        mathsGroup.add(name);
    }

    public static void getCommerceGroupStudent(String name,ArrayList<String> commerceGroup){
        commerceGroup.add(name);
    }

    public static void getTopper(HashMap<String,Float> isTopper){
        float maximumNumber=0;
        for(float e:isTopper.values() ){
            if(maximumNumber<e){
                maximumNumber =e;
            }
        }
        for(String nameOfToppers:getNamesOfTopper(isTopper,maximumNumber)){
            System.out.println(nameOfToppers);
        }
    }

    public static Set<String> getNamesOfTopper(HashMap<String,Float> isTopper, float value){
        Set<String> resultName=new HashSet<>();
        if(isTopper.containsValue(value) ){
            for(Map.Entry<String,Float > entry:isTopper.entrySet() ){
                if(Objects.equals(entry.getValue(),value)){
                    resultName.add(entry.getKey() );
                }
            }
        }
        return resultName;
    }



    public static void main(String[] args) {

        HashMap<String,Float> marksOfAllStudent=new HashMap<>() ;
        ArrayList<String> computerGroupStudent=new ArrayList<>() ;
        ArrayList<String> mathsGroupStudent=new ArrayList<>() ;
        ArrayList<String> commerceGroupStudent=new ArrayList<>() ;
        Scanner scan=new Scanner(System.in) ;
        String i="Start";
        while(!i.equals("exit")){
            System.out.println("Enter name of student");
            String nameOfStudent= scan.next();
            HashMap <String,Float> subjectAndMarks=new HashMap<>() ;
            float totalMarksOfSubject=0;
            System.out.println("Enter Marks Of Mathematics, Commerce, Physics");
            for(int j=0;j<3;j++){
                System.out.println("Enter name of Subject");
                String nameOfSubject= scan.next();
                System.out.println("Enter Marks");
                float marksOfSubject= scan.nextInt();
                totalMarksOfSubject +=marksOfSubject ;
                subjectAndMarks.put(nameOfSubject,marksOfSubject );
            }
            float averageMarksOfSubject=totalMarksOfSubject /3;
            marksOfAllStudent .put(nameOfStudent,averageMarksOfSubject );
            System.out.println("Enter 1 :To put student in Computer Stream");
            System.out.println("Enter 2 :To put student in Maths Stream");
            System.out.println("Enter 3 :To put student in Commerce Stream");
            int stream= scan.nextInt();
            switch(stream){
                case 1:
                    if(averageMarksOfSubject>70 & subjectAndMarks.get("Computer")>80 ){
                        getComputerGroupStudent(nameOfStudent,computerGroupStudent );
                    }
                    else{
                        System.out.println("Marks is less than the required marks");
                    }
                    break;
                case 2:
                    if(averageMarksOfSubject>70 ){
                        getMathsGroupStudent(nameOfStudent,mathsGroupStudent);
                    }
                    else{
                        System.out.println("Marks is less than the required marks");
                    }
                    break;
                case 3:
                    if(subjectAndMarks.get("Mathematics") >80 ){
                        getCommerceGroupStudent(nameOfStudent,commerceGroupStudent) ;
                    }
                    else{
                        System.out.println("Marks is less than the required marks");
                    }
                    break;
            }
            System.out.println("If wants to exit Type:  exit");
            String isExit=scan.next() ;
            if(isExit.equals("exit") ){
                i="exit";
            }
        }

        System.out.println("Name of Students in Computer Stream");
        System.out.println(computerGroupStudent);
        System.out.println("Name of Students in Maths Stream");
        System.out.println(mathsGroupStudent);
        System.out.println("Name of Students in Commerce Stream");
        System.out.println(commerceGroupStudent);

        System.out.println("Name of Students got highest marks");
        getTopper(marksOfAllStudent) ;
    }
}


